﻿
#include <iostream> 
#include <iomanip> 
#include "windows.h"
#include "time.h"

using namespace std;
#define n 10
#define m 5

void ini(int r[n][m])
{
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			r[i][j] = rand() % 48 - 13;
}


int sum(int r[n][m])
{
	int sum = 0;
	for (int i = 0; i < n; i += 2) 
		for (int j = 0; j < m; j++) 
			sum += r[i][j];
	return sum;
}


void print(int r[n][m])
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
			cout << setw(5) << r[i][j];
		cout << endl;
	}
}


int main()
{
	int c[n][m];
	ini(c);
	sum(c);
	print(c);
	cout << "sum = " << sum(c) << endl;
	system("pause");
	return 0;
}
